#coding: utf-8
import os, sys, requests, random
ISO = 'ISO'
IPs = 'IPs'
ISOList = []
Ranges = []
Result = []

if os.path.isfile(IPs):
	os.remove(IPs)

with open(ISO, 'r') as s:
    ISOList = s.read().splitlines()
    random.shuffle(ISOList)

for i in range(0, len(ISOList)):
	send=requests.post('http://ipdeny.com/ipblocks/data/countries/'+ISOList[i]+'.zone')
	Ranges.append(send.text.split('\n'))
	Ranges[i].pop(len(Ranges[i])-1)
	Result.extend(Ranges[i])
	sys.stdout.write(ISOList[i]+' ')
	sys.stdout.flush()

random.shuffle(Result)

with open(IPs, 'a') as p:
	p.write('\n'.join(Result))

sys.stdout.write("\n")